/* global require, PluginHost */

require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	function pause(vid) {
		vid.removeAttribute("autoplay");
		vid.pause();
	}

	function pause_all(row) {
		[...row.querySelectorAll("video")].forEach((vid) => pause(vid));
	}

	ready(function () {
		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED_CDM, function (row) {
			pause_all(row);
			return true;
		});

		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED, function (row) {
			pause_all(row);
			return true;
		});
	});
});
